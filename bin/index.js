const shell = require('shelljs')
const menu = require('./functions');

const _widevine = '--widevine'
const _tray = '--tray'
const _single_instance = '--single-instance'
const _internal_urls = '--internal-urls ".*?"'
const _es3_apis = '--enable-es3-apis'
const _user_agent = '--user-agent "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36"'
const _disable_gpu = '--disable-gpu'
const _background_color = '--background-color "#333333"'


menu([
    {
        hotkey: '01',
        app_name: '4Anime',
        title: '4anime',
        flags: '--tray --enable-es3-apis --single-instance --disable-gpu',
        url: 'https://4anime.to/',
        selected: true
    },
    {
        hotkey: '02',
        app_name: 'Crunchyroll',
        title: 'crunchyroll',
        flags: '--tray --enable-es3-apis --single-instance --disable-gpu',
        url: 'https://www.crunchyroll.com/',
    },
    {
        hotkey: '03',
        app_name: 'Shudder',
        title: "shudder",
        flags: '--tray --enable-es3-apis --single-instance --disable-gpu --widevine --user-agent "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36"',
        url: 'https://www.shudder.com/'
    },
    {
        hotkey: '04',
        app_name: 'Netflix',
        title: 'netflix',
        flags: '--tray --enable-es3-apis --single-instance --disable-gpu --widevine --user-agent "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36"',
        url: 'https://www.netflix.com/'
    },
    {
        hotkey: '05',
        app_name: 'Hulu',
        title: 'hulu',
        flags: '--tray --enable-es3-apis --single-instance --disable-gpu --widevine --user-agent "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36"',
        url: 'https://www.hulu.com/'
    },
    {
        hotkey: '06',
        app_name: 'HBO Max',
        title: 'hbomax',
        flags: '--tray --enable-es3-apis --single-instance --disable-gpu --widevine --user-agent "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36"',
        url: 'https://play.hbomax.com/'
    },
    {
        hotkey: '07',
        app_name: 'Disney Plus',
        title: 'disneyplus',
        flags: '--tray --enable-es3-apis --single-instance --disable-gpu --widevine --user-agent "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36"',
        url: 'https://www.disneyplus.com/'
    },
    {
        hotkey: '08',
        app_name: 'Webtoons',
        title: 'webtoons',
        flags: '--tray --enable-es3-apis --single-instance --disable-gpu',
        url: 'https://www.webtoons.com/'
    },
    {
        hotkey: '09',
        app_name: 'Toomics',
        title: 'toomics',
        flags: '--tray --enable-es3-apis --single-instance --disable-gpu',
        url: 'https://global.toomics.com/'
    },
    {
        hotkey: '10',
        app_name: 'YouTube',
        title: 'youtube',
        flags: '--tray --enable-es3-apis --single-instance --disable-gpu',
        url: 'https://www.youtube.com/'
    },
    {
        hotkey: '11',
        app_name: 'YouTube Music',
        title: 'youtubemusic',
        flags: '--tray --enable-es3-apis --single-instance --disable-gpu',
        url: 'https://music.youtube.com/'
    },
    {
        hotkey: '12',
        app_name: 'YouTube Studio',
        title: 'youtubestudio',
        flags: '--tray --enable-es3-apis --single-instance --disable-gpu',
        url: 'https://studio.youtube.com/'
    },
    {
        hotkey: '13',
        app_name: 'AllBad.Cards',
        title: 'allbadcards',
        flags: '--tray --enable-es3-apis --single-instance --disable-gpu',
        url: 'https://allbad.cards/'
    },
    {
        hotkey: '14',
        app_name: 'Skribbl.io',
        title: 'skribblio',
        flags: '--tray --enable-es3-apis --single-instance --disable-gpu',
        url: 'https://skribbl.io/'
    },
    {
        hotkey: '15',
        app_name: 'DragonBound',
        title: 'dragonbound',
        flags: '--tray --enable-es3-apis --single-instance --disable-gpu',
        url: 'https://dragonbound.net/'
    },
    {
        hotkey: '16',
        app_name: 'PretendYoureXYZ',
        title: 'pretendyourexyz',
        flags: '--tray --enable-es3-apis --single-instance --disable-gpu',
        url: 'https://pyx-1.pretendyoure.xyz/'
    },
    {
        hotkey: '17',
        app_name: 'Stadia',
        title: 'stadia',
        flags: '--tray --enable-es3-apis --single-instance --disable-gpu',
        url: 'https://stadia.google.com/'
    },
    {
        hotkey: '18',
        app_name: 'Doujins',
        title: 'doujins',
        flags: '--tray --enable-es3-apis --single-instance --disable-gpu',
        url: 'https://www.doujins.com/'
    },
    {
        hotkey: '19',
        app_name: 'HAnime',
        title: 'hanime',
        flags: '--tray --enable-es3-apis --single-instance --disable-gpu',
        url: 'https://hanime.tv/'
    },
    {
        hotkey: '20',
        app_name: 'Pornhub',
        title: 'pornhub',
        flags: '--tray --enable-es3-apis --single-instance --disable-gpu',
        url: 'https://www.Pornhub.com/'
    },
    {
        hotkey: '21',
        app_name: 'NHentai',
        title: 'nhentai',
        flags: '--tray --enable-es3-apis --single-instance --disable-gpu',
        url: 'https://nhentai.net/'
    },
    {
        hotkey: '22',
        app_name: 'Cunt Empire',
        title: 'cuntempire',
        flags: '--tray --enable-es3-apis --single-instance --disable-gpu',
        url: 'https://www.cuntempire.com/'
    },
    {
        hotkey: '23',
        app_name: 'Virtual Customs',
        title: 'virtualcustoms',
        flags: '--tray --enable-es3-apis --single-instance --disable-gpu',
        url: 'https://virtualcustoms.net/'
    },
    {
        hotkey: '24',
        app_name: 'Proton Mail',
        title: 'protonmail',
        flags: '--tray --enable-es3-apis --single-instance --disable-gpu',
        url: 'https://mail.protonmail.com/'
    }],
    {
        header: 'Native Apps Compiler',
        border: true,
    }).then(item => {
        if (item) {
            shell.exec(
                `nativefier --name "${item.title}" "${item.url}" ${item.flags} ${_background_color} ${_internal_urls} --icon "C:\\NativeAppsCompiler\\bin\\res\\icons\\${item.title}.ico" "%userprofile%\\AppData\\Local\\Native Apps\\${item.app_name}" && powershell "$s=(New-Object -COM WScript.Shell).CreateShortcut('%userprofile%\\Desktop\\${item.app_name}.lnk');$s.Targetpath='%userprofile%\\AppData\\Local\\Native Apps\\${item.app_name}\\${item.title}-win32-x64\\${item.title}.exe';$s.Save()" && powershell "$s=(New-Object -COM WScript.Shell).CreateShortcut('%userprofile%\\AppData\\Roaming\\Microsoft\\Windows\\Start Menu\\Programs\\${item.app_name}.lnk');$s.Targetpath='%userprofile%\\AppData\\Local\\Native Apps\\${item.app_name}\\${item.title}-win32-x64\\${item.title}.exe';$s.Save()"`, function (code, stdout, stderr) {
                    console.log('Exit code:', code);
                    console.log('Program output:', stdout);
                    console.log('Program stderr:', stderr);
                })
        } else { console.log('You cancelled the menu.'); }
    })